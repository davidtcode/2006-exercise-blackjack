package dt.casino.card;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class DeckTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {}

	@Before
	public void setUp() throws Exception {}

	@After
	public void tearDown() throws Exception {}

	@Test
	public final void testDeck() {
	}

	@Test
	public final void testSize() {
		
		Deck deck = new Deck();
		
		assertEquals(52, deck.size());
		
		deck.deal();
		deck.deal();
		deck.deal();
		
		assertEquals(49, deck.size());
		
		// Drain the deck and test its empty
		for(int i = 0; i < 100; i++)
			deck.deal();
		
		assertEquals(0, deck.size());
	}

	@Test
	public final void testPopulate() {
		
		Deck deck = new Deck();

		deck.populate();
		
		assertEquals(52, deck.size());
	}

	@Test
	public final void testShuffle() {
	
		Deck deck = new Deck();
		
		assertTrue(deck.size() > 0);
		
		List<Card> cardsBefore = deck.getCards();

		deck.shuffle();
		
		List<Card> cardsAfter = deck.getCards();
		
		// Now compare the 3 list of cards
		
		assertTrue(cardsBefore.size() > 0);
		assertTrue(cardsAfter.size() > 0);
		assertEquals("The 2 lists of cards should be of the same size", 
				cardsBefore.size(),
				cardsAfter.size());
		
		boolean atLeastOneCardDiffers = false;
		for(int i = 0; i < cardsBefore.size(); i++){
			
			Card cardBefore = cardsBefore.get(i);
			Card cardAfter = cardsAfter.get(i);
			
			assertNotNull(cardBefore);
			assertNotNull(cardAfter);
			
			if( !(cardBefore.getRank() == cardAfter.getRank()) || 
				!(cardBefore.getSuit() == cardAfter.getSuit())){
				atLeastOneCardDiffers = true;
				
				break;
			}			
		}
		
		// For a "good" shuffle we'll stipulate that at least one card in the card 
		// should be okay. Of course, it is possible a shuffle could result in an
		// identical deck ... but a broken test is worth that risk.
		
		assertTrue("The shuffle has left 2 identical decks", atLeastOneCardDiffers);
	}

	@Test
	public final void testDeal() {
		
		Deck deck = new Deck();
		
		assertTrue(deck.size() > 0);
		
		assertNotNull(deck.deal());
		
		// Drain the deck and test it deals a null
		for(int i = 0; i < 100; i++)
			deck.deal();
		
		assertNull("Deck should deal a null", deck.deal());
	}
}
package dt.casino.card;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import dt.casino.card.Card.Rank;
import dt.casino.card.Card.Suit;

public class HandTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {}

	@Before
	public void setUp() throws Exception {}

	@After
	public void tearDown() throws Exception {}

	@Test
	public final void testClear() {

		Hand hand = new Hand();
		
		assertEquals(0, hand.size());
		
		hand.add(new Card(Rank.EIGHT, Suit.DIAMONDS));
		hand.add(new Card(Rank.TWO, Suit.DIAMONDS));
		
		assertEquals(2, hand.size());
		
		hand.clear();
		
		assertEquals(0, hand.size());
	}

	@Test
	public final void testAdd() {
		
		Hand hand = new Hand();
		
		assertEquals(0, hand.size());
		
		hand.add(new Card(Rank.EIGHT, Suit.DIAMONDS));
		hand.add(new Card(Rank.TWO, Suit.DIAMONDS));
		
		assertEquals(2, hand.size());
	}

	@Test
	public final void testIsBust() {
		
		Hand hand = new Hand();
		
		assertFalse("The hand shouldn't be bust", hand.isBust());
		
		hand.add(new Card(Rank.TEN, Suit.DIAMONDS));
		hand.add(new Card(Rank.TEN, Suit.SPADES));
		hand.add(new Card(Rank.TEN, Suit.HEARTS)); // that's 30 now
		
		assertEquals(3, hand.size());
		
		assertTrue("The hand should be bust", hand.isBust());
	}

	@Test
	public final void testFirstCard() {
		
		Hand hand = new Hand();
		
		hand.add(new Card(Rank.EIGHT, Suit.DIAMONDS));
		hand.add(new Card(Rank.TWO, Suit.SPADES));
		hand.add(new Card(Rank.SIX, Suit.HEARTS));
		hand.add(new Card(Rank.FOUR, Suit.CLUBS));

		Card firstCard = hand.firstCard();
		
		assertTrue("The rank should be 8", firstCard.getRank() == Rank.EIGHT);
		assertTrue("The suit should be diamonds", firstCard.getSuit() == Suit.DIAMONDS);
	}

	@Test
	public final void testSize() {
		
		Hand hand = new Hand();
		
		assertEquals(0, hand.size());
		
		hand.add(new Card(Rank.EIGHT, Suit.DIAMONDS));
		hand.add(new Card(Rank.TWO, Suit.DIAMONDS));
		hand.add(new Card(Rank.SIX, Suit.DIAMONDS));
		hand.add(new Card(Rank.FOUR, Suit.DIAMONDS));
		
		assertEquals(4, hand.size());
	}

	@Test
	public final void testContainsAce() {
		
		Hand hand = new Hand();
		
		assertFalse("Shouldn't contain an ace", hand.containsAce());
		
		hand.add(new Card(Rank.EIGHT, Suit.DIAMONDS));
		
		assertFalse("Shouldn't contain an ace", hand.containsAce());
				
		hand.add(new Card(Rank.TWO, Suit.DIAMONDS));
		hand.add(new Card(Rank.ACE, Suit.DIAMONDS));
		hand.add(new Card(Rank.FOUR, Suit.DIAMONDS));
		
		assertTrue("Should contain an ace", hand.containsAce());
	}

	@Test
	public final void testValue() {
		
		Hand hand = new Hand();
		
		assertEquals(0, hand.value());
		
		hand.add(new Card(Rank.EIGHT, Suit.DIAMONDS));
		
		assertEquals(8, hand.value());

		hand.clear();
		hand.add(new Card(Rank.TWO, Suit.DIAMONDS));
		hand.add(new Card(Rank.ACE, Suit.DIAMONDS));
		hand.add(new Card(Rank.FOUR, Suit.DIAMONDS));
		
		assertEquals(17, hand.value());
	}

	@Test
	public final void testCompareValue() {

		Hand otherHand = new Hand();
		otherHand.add(new Card(Rank.TWO, Suit.DIAMONDS));
		otherHand.add(new Card(Rank.KING, Suit.DIAMONDS));
		
		Hand hand = new Hand();
		
		// The other hand is greater
		hand.add(new Card(Rank.EIGHT, Suit.DIAMONDS));
		
		assertEquals(-1, hand.compareValue(otherHand));

		// Where the value is the same
		hand.clear();
		hand.add(new Card(Rank.TWO, Suit.DIAMONDS));
		hand.add(new Card(Rank.KING, Suit.DIAMONDS));
		
		assertEquals(0, hand.compareValue(otherHand));

		// Where the other hand is lesser in value
		hand.clear();
		hand.add(new Card(Rank.QUEEN, Suit.DIAMONDS));
		hand.add(new Card(Rank.KING, Suit.DIAMONDS));
		
		assertEquals(1, hand.compareValue(otherHand));
	}
}
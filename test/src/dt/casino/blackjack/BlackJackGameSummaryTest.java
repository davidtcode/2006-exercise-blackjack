package dt.casino.blackjack;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class BlackJackGameSummaryTest {

	private BlackJackGameSummary summary = null;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		summary = new BlackJackGameSummary();		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testGetTotalNumberOfGames() {
	}

	@Test
	public final void testSetTotalNumberOfGames() {
	}

	@Test
	public final void testGamePlayed() {
		
		summary.gamePlayed();
		summary.gamePlayed();
		summary.gamePlayed();
		
		assertEquals(3, summary.getTotalNumberOfGames());
	}

	@Test
	public final void testDealerWins() {
		
		assertEquals(0, summary.getDealerWinCount());
		
		summary.dealerWins();
		summary.dealerWins();
		summary.dealerWins();
		summary.dealerWins();
		
		assertEquals(4, summary.getDealerWinCount());
	}

	@Test
	public final void testPlayerWins() {
		
		assertEquals(0, summary.getPlayerWinCount());
		
		summary.playerWins();
		summary.playerWins();
		summary.playerWins();
		summary.playerWins();
		
		assertEquals(4, summary.getPlayerWinCount());
		
	}

	@Test
	public final void testGameDrawn() {
		
		assertEquals(0, summary.getNumberOfGamesDrawn());
		
		summary.gameDrawn();
		summary.gameDrawn();
		summary.gameDrawn();
		summary.gameDrawn();
		summary.gameDrawn();
		summary.gameDrawn();
		summary.gameDrawn();
		summary.gameDrawn();
		summary.gameDrawn();
		summary.gameDrawn();
		
		assertEquals(10, summary.getNumberOfGamesDrawn());
		
	}

	@Test
	public final void testReset() {
		
		summary.gameDrawn();
		summary.gameDrawn();
		summary.dealerWins();
		summary.playerWins();
		summary.gamePlayed();
		
		assertEquals(2, summary.getNumberOfGamesDrawn());
		assertEquals(1, summary.getDealerWinCount());
		assertEquals(1, summary.getPlayerWinCount());
		assertEquals(1, summary.getTotalNumberOfGames());
		
		summary.reset();
		
		assertEquals(0, summary.getNumberOfGamesDrawn());
		assertEquals(0, summary.getDealerWinCount());
		assertEquals(0, summary.getPlayerWinCount());
		assertEquals(0, summary.getTotalNumberOfGames());		
	}
}
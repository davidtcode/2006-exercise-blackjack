package dt.casino.blackjack;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import dt.casino.blackjack.BlackJackGame;
import dt.casino.blackjack.BlackJackGameFactory;
import dt.casino.blackjack.BlackJackGameType;
import dt.casino.blackjack.NoSuchGameExistsException;
import dt.casino.blackjack.impl.BlackJackHard17Impl;
import dt.casino.blackjack.impl.BlackJackSoft17Impl;

public class BlackJackGameFactoryTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {}

	@Before
	public void setUp() throws Exception {}

	@After
	public void tearDown() throws Exception {}

	@Test
	public final void testBuildGame() {
		
		try{
			
			BlackJackGame blackJackGame = BlackJackGameFactory.buildGame(BlackJackGameType.HARD17);
			assertTrue("Game should of of hard17 type", blackJackGame instanceof BlackJackHard17Impl);
			
			blackJackGame = BlackJackGameFactory.buildGame(BlackJackGameType.SOFT17);
			assertTrue("Game should of of soft17 type", blackJackGame instanceof BlackJackSoft17Impl);
		}
		catch(NoSuchGameExistsException ex){
			fail("The blackjack game cannot be found");
		}
	}
}
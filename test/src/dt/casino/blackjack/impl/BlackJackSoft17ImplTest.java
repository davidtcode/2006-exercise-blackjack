package dt.casino.blackjack.impl;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import dt.casino.card.Card;
import dt.casino.card.Deck;
import dt.casino.card.Hand;
import dt.casino.card.Card.Rank;
import dt.casino.card.Card.Suit;

public class BlackJackSoft17ImplTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testCompleteDealerHand() {
		
		BlackJackSoft17Impl soft17Game = new BlackJackSoft17Impl();
		
		Hand dealer = new Hand();
		Deck deck = new Deck();

		// A count > 17 ... should stick
		
		dealer.add(new Card(Rank.TEN, Suit.CLUBS)); 
		dealer.add(new Card(Rank.TEN, Suit.CLUBS));
		
		soft17Game.completeDealerHand(dealer, deck);
		
		assertEquals(2, dealer.size());

		// A count < 17 ... should draw another
		
		dealer = new Hand();
		dealer.add(new Card(Rank.TEN, Suit.CLUBS)); 
		dealer.add(new Card(Rank.TWO, Suit.CLUBS));
		
		soft17Game.completeDealerHand(dealer, deck);
		
		assertTrue(dealer.size() > 2);

		// A count of 17, with NO ace ... should stick
		
		dealer = new Hand();
		dealer.add(new Card(Rank.TEN, Suit.CLUBS)); 
		dealer.add(new Card(Rank.SEVEN, Suit.CLUBS));
		
		soft17Game.completeDealerHand(dealer, deck);
		
		assertEquals(2, dealer.size());
		
		// A count of 17, with an ace ... should draw more cards
		
		dealer = new Hand();
		dealer.add(new Card(Rank.ACE, Suit.CLUBS)); 
		dealer.add(new Card(Rank.SIX, Suit.CLUBS));
		
		soft17Game.completeDealerHand(dealer, deck);
		
		assertTrue(dealer.size() > 2);
	}
}
package dt.casino.blackjack;

/**
 * A class which summaries one or more BlackJack games.
 * 
 * @author David
 */
public class BlackJackGameSummary {

	private int totalNumberOfGames = 0;

	private int dealerWinCount = 0;

	private int playerWinCount = 0;

	private int numberOfGamesDrawn = 0;

	/**
	 * Gets the total number of games played.
	 * 
	 * @return The total number of games played.
	 */
	public int getTotalNumberOfGames() {
		return totalNumberOfGames;
	}

	/**
	 * Sets the total number of games played.
	 * 
	 * @param totalNumberOfGames	The total number of games played.
	 */
	public void setTotalNumberOfGames(int totalNumberOfGames) {
		this.totalNumberOfGames = totalNumberOfGames;
	}

	/**
	 * Makes a game as being played
	 * 
	 * @return	The total number of games now played.
	 */
	public int gamePlayed() {
		return ++this.totalNumberOfGames;
	}

	/**
	 * Gets the number of wins for the dealer.
	 * 
	 * @return The number of wins for the dealer.
	 */
	public int getDealerWinCount() {
		return dealerWinCount;
	}

	/**
	 * Set the number of wins for the dealer.
	 * 
	 * @param dealerWinCount	The number of wins for the dealer.
	 */
	public void setDealerWinCount(int dealerWinCount) {
		this.dealerWinCount = dealerWinCount;
	}

	/**
	 * Marks a game as being won by the dealer.
	 * 
	 * @return	The number of total wins for the dealer.
	 */
	public int dealerWins() {
		return ++this.dealerWinCount;
	}

	/**
	 * Gets the number of wins for the player.
	 * 
	 * @return The number of wins for the player.
	 */
	public int getPlayerWinCount() {
		return playerWinCount;
	}

	/**
	 * Set the number of wins for the player.
	 * 
	 * @param playerWinCount	The number of wins for the player.
	 */	
	public void setPlayerWinCount(int playerWinCount) {
		this.playerWinCount = playerWinCount;
	}

	/**
	 * Marks a game as having being won by the player.
	 * 
	 * @return	The number of total wins for the player.
	 */	
	public int playerWins() {
		return ++this.playerWinCount;
	}

	/**
	 * Gets the number of games which have been drawn.
	 * 
	 * @return The number of games which have been drawn.
	 */
	public int getNumberOfGamesDrawn() {
		return numberOfGamesDrawn;
	}

	/**
	 * Sets the number of games which have been drawn.
	 * 
	 * @param numberOfGamesDrawn The number of games which have been drawn.
	 */
	public void setNumberOfGamesDrawn(int numberOfGamesDrawn) {
		this.numberOfGamesDrawn = numberOfGamesDrawn;
	}

	/**
	 * Marks a game as having been drawn.
	 * 
	 * @return The number of games which have been drawn.
	 */
	public int gameDrawn() {
		return ++this.numberOfGamesDrawn;
	}
	
	/**
	 * Resets all counts.
	 */
	public void reset(){
		this.dealerWinCount = 0;
		this.playerWinCount = 0;
		this.totalNumberOfGames = 0;
		this.numberOfGamesDrawn = 0;
	}
}
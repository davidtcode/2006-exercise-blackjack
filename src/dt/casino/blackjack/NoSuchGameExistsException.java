package dt.casino.blackjack;

/**
 * A generic exception class for handling exceptions relating to a certain
 * game not existing.
 *
 * @author David
 */
public class NoSuchGameExistsException extends Exception {

	/**
	 * Basic constructor, takes an error messages
	 * 
	 * @param reason	The error message
	 */
	public NoSuchGameExistsException(String reason) {
		super(reason);
	}

	/**
	 * Constructor that takes a reason and the cause.
	 * 
	 * @param reason	The <code>String</code> reason why this occurred.
	 * @param cause	The exception that caused this to occur.
	 */
	public NoSuchGameExistsException(String reason, Throwable cause) {
		super(reason, cause);
	}
	
	/**
	 * A default serial number for the class.
	 */
	private static final long serialVersionUID = 1L;	
}
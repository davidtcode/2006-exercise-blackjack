package dt.casino.blackjack.impl;

import org.apache.log4j.Logger;

import dt.casino.blackjack.BlackJackGameAbstract;
import dt.casino.card.Deck;
import dt.casino.card.Hand;

/**
 * A BlackJack variation in which the dealer draws on 17, only if the hand
 * contains an ace.
 * 
 * <p/>
 * 
 * This offers a slight advantage to the dealer.
 * 
 * @author David
 */
public class BlackJackSoft17Impl extends BlackJackGameAbstract {

	private final static Logger log = Logger
			.getLogger(BlackJackSoft17Impl.class);

	/** {@inheritDoc} */
	@Override
	protected void completeDealerHand(Hand dealer, Deck deck) {

		if (log.isDebugEnabled())
			log.debug("completeDealerHand: dealer's hand before is "
					+ dealer.toString());

		while (true) {

			if (dealer.value() > DEALER_HARD_NUMBER)
				break;

			if (!dealer.containsAce() && (dealer.value() == DEALER_HARD_NUMBER))
				break;

			dealer.add(deck.deal());
		}

		if (log.isDebugEnabled())
			log.debug("completeDealerHand: dealer's hand after is "
					+ dealer.toString());

	}
}
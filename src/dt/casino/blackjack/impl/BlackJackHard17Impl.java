package dt.casino.blackjack.impl;

import org.apache.log4j.Logger;

import dt.casino.blackjack.BlackJackGameAbstract;
import dt.casino.card.Deck;
import dt.casino.card.Hand;

/**
 * A BlackJack variation in which the dealer stands on 17 regardless of whether
 * his hand includes an ace or not. This is typically the default behavior for
 * dealer.
 * 
 * @author David
 */
public class BlackJackHard17Impl extends BlackJackGameAbstract {

	private final static Logger log = Logger
			.getLogger(BlackJackHard17Impl.class);

	/** {@inheritDoc} */
	@Override
	protected void completeDealerHand(Hand dealer, Deck deck) {

		if (log.isDebugEnabled())
			log.debug("completeDealerHand: dealer's hand before is "
					+ dealer.toString());

		/*
		 * The options open to the dealer are simple. If cards.value() < 17 he
		 * must draw another card. If cards.value() >= 17, he must stand.
		 */
		while ((dealer.value() < DEALER_HARD_NUMBER)
				&& (dealer.size() <= Hand.MAX_HAND_SIZE)) {
			dealer.add(deck.deal());
		}

		if (log.isDebugEnabled())
			log.debug("completeDealerHand: dealer's hand after is "
					+ dealer.toString());

	}
}
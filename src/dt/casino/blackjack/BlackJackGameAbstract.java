package dt.casino.blackjack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

import dt.casino.card.Deck;
import dt.casino.card.Hand;
import dt.casino.card.HandHolderType;
import dt.casino.blackjack.BlackJackGame;

/**
 * Implements {@link BlackJackGame} and provides basic and common functionality
 * for different variations of the game.
 * 
 * <p/>
 * 
 * A deck is created, two hands are dealt, one to the player and one for the dealer 
 * (computer). The player can only view one of the cards in the dealer�s hand. 
 * The player can choose to stick with his cards or to draw more. The aim is to 
 * have a hand of cards with value as close as possible to 21, without going over. 
 * Either he goes bust, in which case he loses, or he sticks. 
 * 
 * <p/>
 * 
 * When he sticks, it is the turn of the dealer to draw or stick. There are 
 * different variations of the rules by which the dealer draws more cards. Two 
 * are implemented here. The first is the hard-17 rule: if the value of the
 * hand is less than 17 he must draw another card, if 17 and over, he must
 * stick. The other is the soft-17 rule: when at a count of 17, the dealer will
 * draw another card if his hand includes an ace, as this can be counted as a 1. 
 * This offers a slight advantage to the dealer. 
 * 
 * <p/>
 * 
 * Should the dealer go bust, the player wins; otherwise the value of the two 
 * hands are compared to determine who is closest to 21. Ties are possible of course. 
 * 
 * @author David
 */
public abstract class BlackJackGameAbstract implements BlackJackGame {

	final static Logger log = Logger.getLogger(BlackJackGameAbstract.class);
	
	protected BlackJackGameSummary gameSummary = new BlackJackGameSummary();
	
	/** {@inheritDoc} */	
	public void play() {

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		
		String playerChoice;

		gameSummary.reset();
		try{
			
			do {

				playSingleGame(input);

				output("");
				output("Would you like another game, (y)es or (n)o?");

				playerChoice = input.readLine();

				if (log.isDebugEnabled())
					log.debug("play: players choice is " + playerChoice);
				
				if (playerChoice.equalsIgnoreCase("y") == false)
					break;

			} while (true);
		} catch (IOException ex) {
			output("");
			output("Sorry, there has been an problem reading your input. Exiting game.");

			log.error("There has been an IO error", ex);
			
			return;
		}
		finally{
			try {
				input.close();
			} catch (IOException ex) {
				log.error("There has been an IO error when closing a resource", ex);
			}
		}
		
		displayGameSummary();
	}

	/**
	 * Whereas the {@link #play()} method really plays a session of games this
	 * method is responsible for a single game.
	 * 
	 * @param input	The reader to collect input from the player.
	 * 
	 * @throws IOException	Thrown if there is a problem reading and writing to 
	 * and from the console.
	 */
	protected void playSingleGame(BufferedReader input) 
			throws IOException {

		gameSummary.gamePlayed();
		
		output("");
		output("");
		output("#############################################################");
		output("Let's play blackjack! Here's how the cards were dealt ...");
		output("#############################################################");
		output("");

		Deck deck = new Deck();

		Hand player = new Hand(HandHolderType.PLAYER);
		Hand dealer = new Hand(HandHolderType.DEALER);

		player.clear();
		dealer.clear();

		// Deal the two hands (two cards for player and dealer)
		player.add(deck.deal());
		dealer.add(deck.deal());
		player.add(deck.deal());
		dealer.add(deck.deal());

		// Display the hands, so the player can decide how to proceed
		displayHand(dealer);
		displayHand(player);

		// Let the player play out his hand
		
		completePlayerHand(player, deck, input); 
		
		// Complete the dealers hand (unless the player went bust)
		
		if(player.isBust() == false)
			completeDealerHand(dealer, deck);
		
		// Compare the two hands
		
		compareHands(player, dealer);
	}
	
	/**
	 * Interactively completes the players hand, until they go bust, or stand.
	 * 
	 * @param player	The players hand.
	 * @param deck	The deck of cards.
	 * @param input	The reader to collect input from the player.
	 * 
	 * @throws IOException	Thrown if there is a problem reading and writing to 
	 * and from the console.
	 */
	protected void completePlayerHand(Hand player, Deck deck, BufferedReader input) 
			throws IOException {

		String playerChoice;
		
		while (true) {

			output("Would you like to (s)tand or (d)raw another card?");

			playerChoice = input.readLine();
			
			if (log.isDebugEnabled())
				log.debug("completePlayerHand: The player's choice is |" + playerChoice + "|");
			
			if (playerChoice.equalsIgnoreCase("s"))
				break;
			else if (playerChoice.equals("d")) {

				// Player has decided to draw another card
				player.add(deck.deal());

				displayHand(player);

				if (player.isBust())
					break;
				
			} else {
				
				log.warn("completePlayerHand: the user may have made an input error");
				
				// Player probably made an input error, let them decide again
				continue;
			}
		}
	}
	
	/**
	 * The dealer's hand is treated differently from the players hand. The dealer's
	 * hand is completed once the player has finialised their hand. And whether
	 * the deal decides to stand/draw another card is different for different
	 * variations of BlackJack.
	 * 
	 * @param dealer	The dealer's <code>Hand</code>
	 * @param deck	The deck of cards from which cards will be drawn.
	 */
	protected abstract void completeDealerHand(Hand dealer, Deck deck);
	
	/**
	 * Compares the hand of the player and the dealer.
	 * 
	 * @param player	The player's hand.
	 * @param dealer	The dealer's hand to compare it against.
	 */
	protected void compareHands(Hand player, Hand dealer){

		if (log.isDebugEnabled())
			log.debug("compareHands: player ["+player.value()+"] dealer ["+dealer.value()+"]");
		
		if (player.isBust()) {

			output("");
			output(">>>>>> You lose ... You've gone BUST! <<<<<<");
			output("");

			gameSummary.dealerWins();
		}
		else if (dealer.isBust()) {
			
			output("");
			output(">>>>>> You WIN ... The dealer has gone BUST with "
					+ dealer.size()
					+ " cards with value "
					+ dealer.value() + " <<<<<<");
			output("");

			gameSummary.playerWins();
		}

		//
		// Dealer and player are not bust, and both are standing on the cards 
		// they have. Closest to 21 wins.

		else if (player.compareValue(dealer) == -1) {

			output("");
			System.out
					.println(">>>>>> You LOSE ... dealer has value of "
							+ dealer.value() + " <<<<<<");
			output("");

			gameSummary.dealerWins();
		} else if (player.compareValue(dealer) == 1) {
			
			output("");
			output(">>>>>> You WIN ... dealer has value of "
					+ dealer.value() + " <<<<<<");
			output("");

			gameSummary.playerWins();
		} else {
			
			output("");
			output(">>>>>> The game is TIED! The dealer has value of "
					+ dealer.value() + " <<<<<<");
			output("");
			
			gameSummary.gameDrawn();
		}
	}
	
	/** {@inheritDoc} */
	public BlackJackGameSummary getGameSummary(){
		return this.gameSummary;
	}

	/**
	 * Displays to the console what cards are in the <code>Hand</code>
	 * 
	 * @param hand	The <code>Hand</code> whose cards should be displayed.
	 */
	protected void displayHand(Hand hand) {

		boolean isDealerHand 
			= (hand.getHandHolderType() == HandHolderType.DEALER) ? true : false; 
		
		output("");
		output(isDealerHand ? "Dealer:" : "You:");
		output("---------------------");

		if (isDealerHand) {

			/*
			 * As the hand belongs to the dealer, only display the first card.
			 * BUT, the player should know how many cards the dealer has, so
			 * display the rest in an invisible form.
			 */
			output(hand.firstCard().toString());
			for (int i = 1; i < hand.size(); i++) {
				output("[HIDDEN]");
			}
		} else 
			output(hand.toString());

		if (isDealerHand == false) {
			output(">> Hand value: " + hand.value());
		}
		output("");
	}

	/**
	 * Displays the summary of one or more BlackJack games.
	 */
	public void displayGameSummary() {

		System.out.format(
				"%nNumber of games:  %d" + 
				"%nGames drawn:      %d" +
				"%nYour win count:   %d" +
				"%nDealer win count: %d%n%n", 
				this.gameSummary.getTotalNumberOfGames(),
				this.gameSummary.getNumberOfGamesDrawn(),
				this.gameSummary.getPlayerWinCount(),
				this.gameSummary.getDealerWinCount());
	}

	protected void output(String message){
		System.out.println(message);
	}
	
	/**
	 * This constant represents the number at the dealer must stand
	 */
	public static final int DEALER_HARD_NUMBER = 17;		
}
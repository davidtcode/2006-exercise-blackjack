package dt.casino.blackjack;

import dt.casino.blackjack.impl.BlackJackHard17Impl;
import dt.casino.blackjack.impl.BlackJackSoft17Impl;

/**
 * A factory which makes different variations of the BlackJack game.
 * 
 * @author David
 */
public class BlackJackGameFactory {

	public static BlackJackGame buildGame(BlackJackGameType type) 
			throws NoSuchGameExistsException {

		BlackJackGame blackJackGame = null;
		switch (type) {
			case SOFT17:
				blackJackGame = new BlackJackSoft17Impl();
				break;	
			case HARD17:
				blackJackGame = new BlackJackHard17Impl();
				break;	
			default:
				throw new NoSuchGameExistsException("The required game doesn't exist");
		}
		
		return blackJackGame;
	}
}
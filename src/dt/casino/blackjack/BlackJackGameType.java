package dt.casino.blackjack;

/**
 * Defines the different variations of the BlackJack game.
 * 
 * @author David
 */
public enum BlackJackGameType {

	SOFT17("soft17"), HARD17("hard17");

	private String type;

	/**
	 * A constructor for the enum type.
	 * 
	 * @param type	The type of game.
	 */
	BlackJackGameType(String type) {
		this.type = type;
	}

	/**
	 * Gets the type of game.
	 * 
	 * @return	The type of game.
	 */
	public String getType() {
		return type;
	}
}
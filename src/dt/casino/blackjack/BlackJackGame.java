package dt.casino.blackjack;

/**
 * A general interface which different BlackJack game variations must implement.
 * 
 * @author David
 */
public interface BlackJackGame {

	/**
	 * Initiates a game of BlackJack.
	 */
	public void play();

	/**
	 * Gets a summary of one or more games of BlackJack.
	 * 
	 * @see BlackJackGameSummary
	 * 
	 * @return An <code>BlackJackGameSummary</code> instance containing a
	 *         summary of one or more BlackGame games.
	 */
	public BlackJackGameSummary getGameSummary();
}
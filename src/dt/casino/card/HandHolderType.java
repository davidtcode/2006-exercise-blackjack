package dt.casino.card;

/**
 * Defines the different types of holder of a hand of card.s
 * 
 * @author David
 */
public enum HandHolderType {

	PLAYER("player"), DEALER("dealer");

	private String type;

	/**
	 * A constructor which takes a holder type.
	 * 
	 * @param type	The type of holder.
	 */
	HandHolderType(String type) {
		this.type = type;
	}

	/**
	 * Gets the type of holder.
	 * 
	 * @return	The type of holder of the hand.
	 */
	public String getType() {
		return type;
	}
}
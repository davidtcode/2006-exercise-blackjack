package dt.casino.card;

import java.util.ArrayList;
import java.util.List;

/**
 * A class which representing a hand of cards, each of which is dealt from an 
 * instance of the <code>Deck</code> class.
 *  
 * @see Deck
 * @see Card
 * 
 * @author David
 */
public class Hand {

	public static final int MAX_HAND_SIZE = 5;
	public static final int BLACKJACK_MAX = 21;

	private List<Card> cards = new ArrayList<Card>();

	private HandHolderType handHolderType;
	
	/**
	 * A empty constructor
	 */
	public Hand() {
		handHolderType = HandHolderType.PLAYER;
	}

	/**
	 * A constructor which sets the type of holder of the hand.
	 * 
	 * @param handHolderType	The type of holder of the hand.
	 */
	public Hand(HandHolderType handHolderType) {
		this.handHolderType = handHolderType;
	}

	/**
	 * Empties the hand of all cards.
	 */
	public void clear() {
		cards.clear();
	}

	/**
	 * Adds a Card object to the hand, if there is room.
	 * 
	 * @param card The <code>Card</code> to add to the hand
	 * 
	 * @return True if card was added, otherwise false
	 */
	public boolean add(Card card) {

		if (cards.size() >= MAX_HAND_SIZE) {
			return false;
		}
		
		cards.add(card);

		return true;
	}

	/**
	 * Determines if the hand has gone bust
	 * 
	 * @return True if value greater than 21
	 */
	public boolean isBust() {

		if (this.value() > BLACKJACK_MAX) {
			return true;
		}

		return false;
	}

	/**
	 * Retrieves the card first added to the hand
	 * 
	 * @return The first card in the deck
	 */
	public Card firstCard() {

		if (cards.isEmpty()) {
			return null;
		}

		return cards.get(0);
	}

	/**
	 * Retrieves the number of cards in the hand.
	 * 
	 * @return	the number of cards in the hand
	 */
	public int size() {
		return cards.size();
	}

	/**
	 * Determines if the hand contains an Ace or not.
	 * 
	 * @return True if hand has an Ace, false otherwise
	 */
	public boolean containsAce() {

		Card card;
		for (int i = 0; i < cards.size(); i++) {

			card = cards.get(i);
			if (card.getRank().equals(Card.Rank.ACE)) 
				return true;
			
		}

		return false;
	}

	/**
	 * Gets the value of the hand, found by adding the value of each card in the 
	 * hand.
	 * 
	 * @return The value of the hand.
	 */
	public int value() {

		int value = 0;
		Card card;
		for (int i = 0; i < cards.size(); i++) {

			card = cards.get(i);
			switch (card.getRank()) {
			case TWO:
				value += 2;
				break;
			case THREE:
				value += 3;
				break;
			case FOUR:
				value += 4;
				break;
			case FIVE:
				value += 5;
				break;
			case SIX:
				value += 6;
				break;
			case SEVEN:
				value += 7;
				break;
			case EIGHT:
				value += 8;
				break;
			case NINE:
				value += 9;
				break;
			case TEN:
				value += 10;
				break;
			case JACK:
				value += 10;
				break;
			case QUEEN:
				value += 10;
				break;
			case KING:
				value += 10;
				break;
			case ACE:
				value += 11;
				break;
			default:
				value += 0;
				break;
			}
		}

		/*
		 * Remember, in blackjack, the Ace can count as a 1 or a 11. We will
		 * always assign a value of 11, UNLESS the hand goes bust as a result.
		 * If this is the case, the value for the Ace is set to 1, which amounts
		 * to subtracting 10 from the total hand value.
		 */
		if ((value > BLACKJACK_MAX) && (this.containsAce())) {
			value -= 10;
		}

		return value;
	}

	/**
	 * Compares the blackjack value of this hand against another
	 * 
	 * @param hand The hand to compare against.
	 * 
	 * @return -1 means the comparing hand is of lesser value, 1 greater value
	 *         and 0 the same value
	 */
	public int compareValue(Hand hand) {

		if (this.value() < hand.value())
			return -1;
		else if (this.value() > hand.value())
			return 1;
		else
			return 0; // values are equal
	}

	/**
	 * Gets the type of holder of this hand.
	 * 
	 * @return	The type of holder of this hand.
	 */
	public HandHolderType getHandHolderType() {
		return handHolderType;
	}

	/**
	 * Sets the type of holder of this hand.
	 * 
	 * @param handHolderType	The type of holder of this hand.
	 */
	public void setHandHolderType(HandHolderType handHolderType) {
		this.handHolderType = handHolderType;
	}

	/**
	 * Displays the hand as a string.
	 * 
	 * @return A string representation of the hand
	 */
	public String toString() {

		String str = "";
		for (int i = 0; i < cards.size(); i++) {
			str += cards.get(i).toString();
			str += System.getProperty("line.separator");
		}
		return str;
	}
}
package dt.casino.card;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A class representing a deck of playing cards.
 * 
 * @see Card
 * 
 * @author David
 */
public class Deck {

	private int cardsDealt;

	private List<Card> cards = new ArrayList<Card>();

	/**
	 * A no-parameter constructor.
	 * 
	 * <p/>
	 * 
	 * It populates the deck before "shuffling" it.
	 */
	public Deck() {
		initialise();
	}

	/**
	 * Gets the size of the deck.
	 * 
	 * @return	The size of the deck.
	 */
	public int size() {
		return cards.size();
	}

	/**
	 * Populates the deck with <code>Card</code> objects and then shuffles it.
	 */
	public void initialise() {
		cardsDealt = 0;

		populate();
		shuffle();
	}

	/**
	 * Populates the deck.
	 */
	public void populate() {

		// Lets ensure there are no cards in the deck first
		cards.clear();

		// Now populate the deck
		for (Card.Suit suit : Card.Suit.values()) {
			for (Card.Rank rank : Card.Rank.values()) {
				cards.add(new Card(rank, suit));
			}
		}
	}

	/**
	 * Shuffles the deck, using an algorithm contained in the Collections class.
	 * 
	 * @see <code>java.util.Collections</code>
	 */
	public void shuffle() {
		Collections.shuffle(cards);
	}

	/**
	 * Deals a card from the deck. It effectively "pops" a card from the "top" 
	 * of the deck.
	 * 
	 * @return A Card object from the "top" of the deck. Returns <code>null</code>
	 * if the deck is empty.
	 */
	public Card deal() {

		// If there is no cards left in the deck, return a null
		if (cards.isEmpty()) {
			return null;
		}

		cardsDealt++;
		
		return cards.remove(cards.size() - 1);
	}
	
	/**
	 * Gets the cards in the deck.
	 * 
	 * <p/>
	 * 
	 * The returned list is a clone of the real deck of cards. 
	 * 
	 * @return	A cloned copy of the cards in the deck.
	 */
	public List<Card> getCards(){
		
		List<Card> clonedCards = new ArrayList<Card>();
		
		for(Card card : cards){
			clonedCards.add(
				new Card(card.getRank(), card.getSuit()));
		}
		
		return clonedCards;
	}

	/**
	 * Loops over the deck and constructs a string composed of the
	 * <code>toString</code> returns of each card object.
	 * 
	 * @return A string representation of the deck.
	 */
	public String toString() {

		String str = "";

		for (int i = 0; i < cards.size(); i++) {
			str += cards.get(i).toString();
			str += System.getProperty("line.separator");
		}
		return str;
	}
}
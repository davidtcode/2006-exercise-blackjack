package dt.casino.card;

/**
 * A class which models a playing card. A card is represented by two fields: a 
 * rank which can be anything from an Ace through to a King and suit, which can 
 * be a Club, Diamond, Heart or Spade. 
 * 
 * <p/>
 * 
 * This class does not impose any value on the card itself, such as defining an 
 * Ace as being higher in value than a King. It is left to the usage of this class 
 * to define how cards compare against one another.
 * 
 * <p/>
 * 
 * Two enums, <code>Suit</code> and <code>Rank</code>, define the various suits
 * and ranks a card can have.
 * 
 * @author David
 */
public class Card {

	/**
	 * The 4 suits a card can have.
	 */
	public static enum Suit {
		CLUBS, DIAMONDS, HEARTS, SPADES
	}

	/**
	 * The 13 different ranks a card can have.
	 */
	public static enum Rank {
		TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
	}

	private Rank rank;
	private Suit suit;

	/**
	 * Constructs the card object.
	 * 
	 * @param rank  One of the constants from the Rank enum
	 * @param suit	One of the constants from the Suit enum
	 */
	public Card(Rank rank, Suit suit) {
		this.rank = rank;
		this.suit = suit;
	}

	/**
	 * Gets the rank of this card object.
	 * 
	 * @return The <code>Rank</code> of this card.
	 */
	public Rank getRank() {
		return this.rank;
	}

	/**
	 * Gets the suit of this card object.
	 * 
	 * @return The <code>Suit</code> of the card.
	 */
	public Suit getSuit() {
		return this.suit;
	}

	/**
	 * Gets a string representation of this Card.
	 * 
	 * @return A readable representation of the card object
	 */
	public String toString() {
		String str = "[";

		switch (this.rank) {
		case TWO:
			str += "Two of ";
			break;
		case THREE:
			str += "Three of ";
			break;
		case FOUR:
			str += "Four of ";
			break;
		case FIVE:
			str += "Five of ";
			break;
		case SIX:
			str += "Six of ";
			break;
		case SEVEN:
			str += "Seven of ";
			break;
		case EIGHT:
			str += "Eight of ";
			break;
		case NINE:
			str += "Nine of ";
			break;
		case TEN:
			str += "Ten of ";
			break;
		case JACK:
			str += "Jack of ";
			break;
		case QUEEN:
			str += "Queen of ";
			break;
		case KING:
			str += "King of ";
			break;
		case ACE:
			str += "Ace of ";
			break;
		default:
			str += "Undefined rank ";
			break;
		}

		switch (this.suit) {
		case CLUBS:
			str += "Clubs";
			break;
		case DIAMONDS:
			str += "Diamonds";
			break;
		case HEARTS:
			str += "Hearts";
			break;
		case SPADES:
			str += "Spades";
			break;
		default:
			str += "Undefined suit";
			break;
		}

		str += "]";
		return str;
	}
}
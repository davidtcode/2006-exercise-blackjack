package dt.casino;

import java.io.IOException;

import org.apache.log4j.Logger;

import dt.casino.blackjack.BlackJackGame;
import dt.casino.blackjack.BlackJackGameFactory;
import dt.casino.blackjack.BlackJackGameType;
import dt.casino.blackjack.NoSuchGameExistsException;

/**
 * The main class which kicks off a game of BlackJack.
 * 
 * @author David Tegart
 */
public class Casino {
	
	final static Logger log = Logger.getLogger(Casino.class);
	
	/**
	 * The main method which kicks off the BlackJack game.
	 * 
	 * @param args	The supplied command-line arguments.
	 * 
	 * @throws IOException	Thrown if there is a problem reading and writing to 
	 * and from the console.
	 */
	public static void main(String[] args) throws IOException {

		try {
			
			BlackJackGame blackJackGame = BlackJackGameFactory.buildGame(
					getBlackJackGameType(args));
			
			blackJackGame.play();
			
		} catch (NoSuchGameExistsException ex) {
			
			log.error("The selected blackjack game doesn't exist", ex);
			
			output("Sorry, the selected blackjack game doesn't exist.");
			
			return;
		}
	}

	/**
	 * Extracts the specified type of BlackJack game required. If this is not 
	 * supplied, a default game will be played.
	 * 
	 * @param args	The supplied command-line arguments.
	 * 
	 * @return	A <code>BlackJackGameType</code> instance specifying the type
	 * of game.
	 */
	private static BlackJackGameType getBlackJackGameType(String[] args){

		BlackJackGameType gameType = BlackJackGameType.HARD17;
		
		if((args != null) && (args.length > 0) && (args[0] != null)){
			if(args[0].equalsIgnoreCase(BlackJackGameType.SOFT17.getType())){
				
				if(log.isDebugEnabled())
					log.debug("The supplied argument is " + args[0]);
				
				gameType = BlackJackGameType.SOFT17;
			}
		}

		if(log.isDebugEnabled())
			log.debug("The selected blackjack game type is " + gameType.getType());

		return gameType;
	}
	
	private static void output(String message){
		System.out.println(message);
	}
}
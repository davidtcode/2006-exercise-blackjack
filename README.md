## 2006-exercise-blackjack

Written as part of an interview process in early 2006. The requirement was a command-line version of the card game BlackJack:

* A single "user" or player plays against the program or "dealer".
* At the start of the game the player and dealer are each dealt a hand of two cards. The player can see one of the dealer's cards.
* Players and dealers have options to "stick", i.e. draw no more cards, or continue to draw cards until 5 cards are in the hand. 
* The player aims to have a hand closer to 21 than that of the dealer, without going over 21.

## How to execute it?

* Clone the repository. 
* Install Java 1.5 and Apache Ant (current version of 1.9 works fine).  
* Change directory to base of the repository, where the **build.xml** is and  run the command **ant jar**. 
  * This will build the jar file.
  * It will also execute the JUnit tests, the output of which are at ./test/output 
* To then execute the jar run the command **java -cp .\etc\;lib\log4j-1.2.9.jar;dist\Blackjack.jar dt.casino.Casino soft17**
* This will generate a log4j log file in the same directory.
* You can run 2 variants of the game: soft17 and hard17. One of these can be passed on the command line as shown above.
  * The former involves the dealer (a.k.a the computer) drawing another card if its hand has a value of 17 and includes an ace.
  * Whereas the latter involves the dealer sticking on 17, regardless of whether its hand includes an ace or not.
